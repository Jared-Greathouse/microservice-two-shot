from django.db import models

from django.db import models


class Hat(models.Model):
    fabric = models.CharField(max_length=200)
    style_name = models.CharField(max_length=200)
    color = models.CharField(max_length=200)
    picture_url = models.URLField(null=True)
    location = models.ForeignKey("LocationVO", related_name="hats", on_delete=models.CASCADE, null=True)
    
    def __str__(self):
        return "Hat ID" + str(self.id) + " " + self.style_name 
    
class LocationVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    closet_name = models.CharField(max_length=100)
    section_number = models.PositiveSmallIntegerField(null=True)
    shelf_number = models.PositiveSmallIntegerField(null=True)

    def __str__(self):
        return "Location VO ID : " + str(self.id) + " " + self.closet_name

